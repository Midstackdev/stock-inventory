$(document).ready(function(){

	var DOMAIN = 'http://localhost/inventory/public';

	addNewRow();

	$("#add").click(function(){
		addNewRow();
	});

	function addNewRow(){
		$.ajax({
			url: DOMAIN+'/includes/process.php',
			type: 'POST',
			data: {getNewOrderItem: 1},
			success: function(data){
				$("#invoice_item").append(data);
				var n = 0;
				$(".number").each(function(){
					$(this).html(++n);
				});
			}
		});
				
	}

	$("#remove").click(function(){
		$("#invoice_item").children("tr:last").remove();
		calculate(0,0);
	});

	$("#invoice_item").on("change", ".pid", function(){
		var pid = $(this).val();
		var tr = $(this).parent().parent();
		$(".overlay").show();
		$.ajax({
			url: DOMAIN+'/includes/process.php',
			type: 'POST',
			dataType: 'json',
			data: {getPriceAndQty:1, id:pid},
			success: function(data){
				//console.log(data);
				tr.find(".tqty").val(data["product_stock"]);
				tr.find(".pro_name").val(data["product_name"]);
				tr.find(".qty").val(1);
				tr.find(".price").val(data["product_price"]);
				tr.find(".amt").html( tr.find(".qty").val() * tr.find(".price").val() );
				calculate(0,0);
			}
		});
		
		
	});

	$("#invoice_item").on("keyup", ".qty", function(){
		var qty = $(this);
		var tr = $(this).parent().parent();
		if(isNaN(qty.val())){
			alert("Please enter a valid quantity");
			qty.val(1);
		}else{
			if((qty.val() - 0) > (tr.find(".tqty").val() - 0) ){
				alert("Sorry this much quantity is not available");
				qty.val(1);
			}else{
				tr.find(".amt").html(qty.val() * tr.find(".price").val());
				calculate(0,0);
			}
		}
	});


	function calculate(dis, paid){
		var sub_total = 0;
		var gst = 0;
		var net_total = 0;
		var discount = dis;
		var paid_amt = paid;
		var due = 0;
		$(".amt").each(function(){
			sub_total = sub_total + ($(this).html() * 1);
		});
		gst = parseInt(0.17 * sub_total);
		net_total = gst + sub_total;
		net_total = net_total - dis;
		due = net_total - paid_amt;
		$("#gst").val(gst);
		$("#sub_total").val(sub_total);
		
		$("#discount").val(dis);
		$("#net_total").val(net_total);
		// $("#paid")
		$("#due").val(due); 
	}


	$("#discount").keyup(function(){
		var discount = $(this).val();
		calculate(discount,0);
	});


	$("#paid").keyup(function(){
		var paid = $(this).val();
		var discount = $("#discount").val();
		calculate(discount, paid);
	});


	// order Accepting //
	$("#order_form").click(function(){
		var status = false;
		var name = $("#cust_name");
		var dis = $("#discount");
		var paid = $("#paid");
		var pro = $(".pid");
		var invoice = $("#get_order_data").serialize();

		if(name.val() == ""){
			name.addClass("border-danger");
			$("#n_error").html("<span class='text-danger'>Please Enter Customer Name</span>");
			status = false;
		}else{
			name.removeClass("border-danger");
			$("#n_error").html("");
			status = true;
		}

		if(dis.val() == ""){
			dis.addClass("border-danger");
			$("#d_error").html("<span class='text-danger'>Please Enter A Value</span>");
			status = false;
		}else{
			dis.removeClass("border-danger");
			$("#d_error").html("");
			status = true;
		}

		if(paid.val() == ""){
			paid.addClass("border-danger");
			$("#p_error").html("<span class='text-danger'>Please Enter A Value</span>");
			status = false;
		}else{
			paid.removeClass("border-danger");
			$("#p_error").html("");
			status = true;
		}

		if(pro.val() == ""){
			pro.addClass("border-danger");
			$(".p-error").html("<span class='text-danger'>Please Select A product</span>");
			status = false;
		}else{
			pro.removeClass("border-danger");
			$(".p-error").html("");
			status = true;
		}

		if(name.val() != "" &&  paid.val() !="" && status == true){
			$.ajax({
				url: DOMAIN+'/includes/process.php',
				type: 'POST',
				data: $("#get_order_data").serialize(),
				success : function(data){

					if(data < 0){
						alert(data);

					}else{
						$("#get_order_data").trigger('reset');	

						if(confirm("Do you want to print Invoice ?")){
							window.location.href = DOMAIN+'/includes/invoice_bill.php?invoice_no='+data+'&'+invoice;
						}
						
					}
					
					
				}
			});

		}

		
		
	});






});