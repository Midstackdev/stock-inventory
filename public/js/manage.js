$(document).ready(function(){

	var DOMAIN = 'http://localhost/inventory/public';


	// Manage Category
	manageCategory(1);
	function manageCategory(pn){
		$.ajax({
			url: DOMAIN + '/includes/process.php',
			method: 'POST',
			data: {manageCategory:1, pageno:pn},
			success: function(data){
				$("#get_category").html(data);
			}
		})
	}

	$("body").on("click",".page-link", function(){
		var pn = $(this).attr("pn");
		manageCategory(pn);
	});


	// Delete Category
	$("body").on("click", ".del_cat", function(){
		var did = $(this).attr("did");
		if(confirm("Are you sure ? You want to delete...")){
			$.ajax({
				url: DOMAIN+'/includes/process.php',
				method: 'POST',
				data: {deleteCategory:1, id:did},
				success: function(data){
					if(data == "DEPENDENT_CATEGORY"){
						alert(data + " CANNOT BE DELETED"); 
					}else if(data == "CATEGORY_DELETED"){
						alert(data + " SUCCESSFULLY");
						manageCategory(1);
					}else if(data == "ORDER_DELETED"){
						alert(data + " SUCCESSFULLY");
					}else{
						alert(data);
					}	
				}
			});
		}else{
			
		}
	});

	// fetch category
	fetch_category();
	function fetch_category(){
		$.ajax({
			url: DOMAIN+'/includes/process.php',
			method: 'POST',
			data: {getCategory:1},
			success: function(data){
				var root = "<option value='0'>Root</option>";
				var choose = "<option value=''>Choose Category</option>";
				$("#parent_cat").html(root+data);
				$("#select_cat").html(choose+data);
			}
		});
	}

	// fetch brand
	fetch_brand();
	function fetch_brand(){
		$.ajax({
			url: DOMAIN+'/includes/process.php',
			method: 'POST',
			data: {getBrand:1},
			success: function(data){
				var choose = "<option value=''>Choose Brand</option>";
				$("#select_brand").html(choose+data);
			}
		});
	}

	// Update Category
	$("body").on("click", ".edit_cat", function(){
		var eid = $(this).attr("eid");
		$.ajax({
			url: DOMAIN + '/includes/process.php',
			method: 'POST',
			dataType: 'json',
			data: {updateCategory:1, id:eid},
			success: function(data){
				console.log(data);
				$("#form_category_edit").modal("show");
				$("#cid").val(data["cid"]);
				$("#update_category").val(data["category_name"]);
				$("#parent_cat").val(data["parent_cat"]);
			}
		});
	});

	$("#update_category_form").on("submit", function(){
		if($("#update_category").val() == ""){
			$("#update_category").addClass('border-danger');
			$("#cat_error").html("<span class='text-danger'>Please Enter Category Name</span>"); 
		}else{
			$.ajax({
				url:DOMAIN+"/includes/process.php",
				method: 'POST',
				data: $("#update_category_form").serialize(),
				success: function(data){
					alert(data);
					window.location.href = "";
				}
			});
		}
	})


	// ------------------ Brands-------------------------------------------------
	// Manage Brand
	manageBrand(1);
	function manageBrand(pn){
		$.ajax({
			url: DOMAIN + '/includes/process.php',
			method: 'POST',
			data: {manageBrand:1, pageno:pn},
			success: function(data){
				$("#get_brand").html(data);
			}
		})
	}

	// Brand Pagination
	$("body").on("click",".page-link", function(){
		var pn = $(this).attr("pn");
		manageBrand(pn);
	});

	// Delete Brand
	$("body").on("click", ".del_brand", function(){
		var did = $(this).attr("did");
		if(confirm("Are you sure ? You want to delete...")){
			$.ajax({
				url: DOMAIN+'/includes/process.php',
				method: 'POST',
				data: {deleteBrand:1, id:did},
				success: function(data){
					if(data == "ORDER_DELETED"){
						alert(data + " BRAND"); 
						manageBrand(1);
					}else{
						alert(data);
					}	
				}
			});
		}
	});

	// Update Brand
	$("body").on("click", ".edit_brand", function(){
		var eid = $(this).attr("eid");
		$.ajax({
			url: DOMAIN + '/includes/process.php',
			method: 'POST',
			dataType: 'json',
			data: {updateBrand:1, id:eid},
			success: function(data){
				console.log(data);
				//$("#form_category_edit").modal("show");
				$("#bid").val(data["bid"]);
				$("#update_brand").val(data["brand_name"]);
			}
		});
	});

	$("#update_brand_form").on("submit", function(){
		if($("#update_brand").val() == ""){
			$("#update_brand").addClass('border-danger');
			$("#brand_error").html("<span class='text-danger'>Please Enter Brand Name</span>"); 
		}else{
			$.ajax({
				url:DOMAIN+"/includes/process.php",
				method: 'POST',
				data: $("#update_brand_form").serialize(),
				success: function(data){
					alert(data);
					window.location.href = "";
				}
			});
		}
	});


	// --------------------Products----------------------------------------------

	// Manage Products
	manageProduct(1);
	function manageProduct(pn){
		$.ajax({
			url: DOMAIN + '/includes/process.php',
			method: 'POST',
			data: {manageProduct:1, pageno:pn},
			success: function(data){
				$("#get_product").html(data);
			}
		})
	}

	// Brand Pagination
	$("body").on("click",".page-link", function(){
		var pn = $(this).attr("pn");
		manageProduct(pn);
	});

	// Delete Brand
	$("body").on("click", ".del_product", function(){
		var did = $(this).attr("did");
		if(confirm("Are you sure ? You want to delete...")){
			$.ajax({
				url: DOMAIN+'/includes/process.php',
				method: 'POST',
				data: {deleteProduct:1, id:did},
				success: function(data){
					if(data == "ORDER_DELETED"){
						alert(data + " PRODUCT"); 
						manageProduct(1);
					}else{
						alert(data);
					}	
				}
			});
		}else{
			
		}
	});

	// Update Brand
	$("body").on("click", ".edit_product", function(){
		var eid = $(this).attr("eid");
		$.ajax({
			url: DOMAIN + '/includes/process.php',
			method: 'POST',
			dataType: 'json',
			data: {updateProduct:1, id:eid},
			success: function(data){
				console.log(data);
				//$("#form_category_edit").modal("show");
				$("#pid").val(data["pid"]);
				$("#update_product").val(data["product_name"]);
				$("#select_cat").val(data["cid"]);
				$("#select_brand").val(data["bid"]);
				$("#product_price").val(data["product_price"]);
				$("#product_qty").val(data["product_stock"]);
			}
		});
	});

	// Update Product
	$("#update_product_form").on('submit', function(){
		$.ajax({
			url: DOMAIN +'/includes/process.php',
			method: "POST",
			data: $("#update_product_form").serialize(),
			success: function(data){
				if(data == "UPDATED"){
					alert("PRODUCT "+data);
					window.location.href = "";
				}else{
					alert(data);
				}	
			}
		});
	});


	// Manage Invoice
	manageInvoice(1);
	function manageInvoice(pn){
		$.ajax({
			url: DOMAIN + '/includes/process.php',
			method: 'POST',
			data: {manageInvoice:1, pageno:pn},
			success: function(data){
				$("#get_invoice").html(data);
			}
		})
	}

	$("body").on("click",".page-link", function(){
		var pn = $(this).attr("pn");
		manageInvoice(pn);
	});




});