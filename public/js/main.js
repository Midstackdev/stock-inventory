$(document).ready(function(){

	var DOMAIN = 'http://localhost/inventory/public';
		
	$("#register_form").on("submit", function(){
		var status = false;
		var name = $("#username");
		var email = $("#email");
		var pass = $("#password");
		var cpass = $("#confirm_password");
		var type = $("#usertype");
		// var n_patt = new RegExp(/^[A-Za-z ]+$/);
		var e_patt = new RegExp(/^[a-z0-9_-]+(\.[a-z0-9_-]+)*@[a-z0-9_-]+(\.[a-z0-9_-]+)*(\.[a-z]{2,4})$/);

		if(name.val() == "" || name.val().length < 6){
			name.addClass("border-danger");
			$("#u_error").html("<span class='text-danger'>Please Enter Username and Name must be betweem 6-30 characters</span>");
			status = false;
		}else{
			name.removeClass("border-danger");
			$("#u_error").html("");
			status = true;
		}

		if(!e_patt.test(email.val())){
			email.addClass("border-danger");
			$("#e_error").html("<span class='text-danger'>Please Enter a Valid email</span>");
			status = false;
		}else{
			email.removeClass("border-danger");
			$("#e_error").html("");
			status = true;
		}

		if(pass.val() == "" || pass.val().length < 6){
			pass.addClass("border-danger");
			$("#p_error").html("<span class='text-danger'>Please Enter password and pasword must be 6 or more characters</span>");
			status = false;
		}else{
			pass.removeClass("border-danger");
			$("#p_error").html("");
			status = true;
		}

		if(cpass.val() == "" || cpass.val().length < 6){
			cpass.addClass("border-danger");
			$("#cp_error").html("<span class='text-danger'>Please Enter password and pasword must be 6 or more characters</span>");
			status = false;
		}else{
			cpass.removeClass("border-danger");
			$("#cp_error").html("");
			status = true;
		}

		if(type.val() == ""){
			type.addClass("border-danger");
			$("#t_error").html("<span class='text-danger'>Please Select a User type</span>");
			status = false;
		}else{
			type.removeClass("border-danger");
			$("#t_error").html("");
			status = true;
		}

		if((pass.val() == cpass.val()) && status == true){
			$(".overlay").show();
			$.ajax({
				url: DOMAIN + '/includes/process.php',
				method: 'POST', 
				data: $("#register_form").serialize(),
				success: function(data){
					if(data == "EMAIL_ALREADY_EXISTS"){
						$(".overlay").hide();
						alert("This email is already used");
					}else if(data == "SOME_ERROR"){
						$(".overlay").hide();
						alert("Something went Wrong");
					}else{
						$(".overlay").hide();
						window.location.href = encodeURI(DOMAIN +  '/login.php?msg=You are register Login to Begin');
					}
					
				}
			});
			
		}else{
			cpass.addClass("border-danger");
			$("#cp_error").html("<span class='text-danger'>Passwords do not match</span>");
			status = false;
		}

	});

	// Form Login
	$("#form_login").on('submit', function(){
		var status = false;
		var email = $("#email");
		var pass = $("#password");

		if(email.val() == ""){
			email.addClass("border-danger");
			$("#e_error").html("<span class='text-danger'>Please Enter email address</span>");
			status = false;
		}else{
			email.removeClass("border-danger");
			$("#e_error").html("");
			status = true;
		}

		if(pass.val() == ""){
			pass.addClass("border-danger");
			$("#p_error").html("<span class='text-danger'>Please Enter password</span>");
			status = false;
		}else{
			pass.removeClass("border-danger");
			$("#p_error").html("");
			status = true;
		}

		if(status){
			$(".overlay").show();
			$.ajax({
				url: DOMAIN + '/includes/process.php',
				type: 'POST', 
				data: $("#form_login").serialize(),
				success: function(data){
					if(data == "NOT REGISTERED"){
						$(".overlay").hide();
						email.addClass("border-danger");
						$("#e_error").html("<span class='text-danger'>Your email is not registered</span>");
					}else if(data == "PASSWORD_NOT_MATCHED"){
						$(".overlay").hide();
						pass.addClass("border-danger");
						$("#p_error").html("<span class='text-danger'>Please Enter correct password</span>");
					}else{
						$(".overlay").hide();
						console.log(data);
						window.location.href = encodeURI(DOMAIN +  '/index.php');
					}
					
				}
			});
		}
	});

	// fetch category
	fetch_category();
	function fetch_category(){
		$.ajax({
			url: DOMAIN+'/includes/process.php',
			method: 'POST',
			data: {getCategory:1},
			success: function(data){
				var root = "<option value='0'>Root</option>";
				var choose = "<option value=''>Choose Category</option>";
				$("#parent_cat").html(root+data);
				$("#select_cat").html(choose+data);
			}
		});
	}


	// fetch brand
	fetch_brand();
	function fetch_brand(){
		$.ajax({
			url: DOMAIN+'/includes/process.php',
			method: 'POST',
			data: {getBrand:1},
			success: function(data){
				var choose = "<option value=''>Choose Brand</option>";
				$("#select_brand").html(choose+data);
			}
		});
	}


	// Add Category
	$("#category_form").on("submit", function(){
		if($("#category_name").val() == ""){
			$("#category_name").addClass('border-danger');
			$("#cat_error").html("<span class='text-danger'>Please Enter Category Name</span>"); 
		}else{
			$.ajax({
				url:DOMAIN+"/includes/process.php",
				method: 'POST',
				data: $("#category_form").serialize(),
				success: function(data){
					if(data == "CATEGORY_ADDED"){
						$("#category_name").removeClass('border-danger');
						$("#cat_error").html("<span class='text-success'>"+data+"</span>");
						$("#category_name").val("");
						fetch_category();
					}else{
						alert(data);
					}
				}
			});
		}

	});


	// Add Brand
	$("#brand_form").on("submit", function(){
		if($("#brand_name").val() == ""){
			$("#brand_name").addClass('border-danger');
			$("#brand_error").html("<span class='text-danger'>Please Enter Brand Name</span>");
		}else{
			$.ajax({
				url: DOMAIN+'/includes/process.php',
				method: 'POST',
				data: $("#brand_form").serialize(),
				success: function(data){
					if(data == "BRAND_ADDED"){
						$("#brand_name").removeClass('border-danger');
						$("#brand_error").html("<span class='text-success'>"+data+"</span>");
						$("#brand_name").val("");
						fetch_brand();
					}else{
						alert(data);
					}
				}
			});
		}
	});


	// Add Product
	$("#product_form").on('submit', function(){
		$.ajax({
			url: DOMAIN +'/includes/process.php',
			method: "POST",
			data: $("#product_form").serialize(),
			success: function(data){
				if(data == "NEW_PRODUCT_ADDED"){
						alert(data);
						$("#product_name").val("");
						$("#select_cat").val("");
						$("#select_brand").val("");
						$("#product_price").val("");
						$("#product_qty").val("");
					}else{
						console.log(data);
					}
			}
		});
	});


	// Manage Category
	// manageCategory();
	// function manageCategory(){
	// 	$.ajax({
	// 		url: DOMAIN + '/includes/process.php',
	// 		method: 'POST',
	// 		data: {manageCategory:1},
	// 		success: function(data){
	// 			$("#get_category").html(data);
	// 		}
	// 	})
	// }



});