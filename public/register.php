<!DOCTYPE html>
<html lang="en">
<head>
	<meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
	<title>Inventory Management System</title>
	<link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.1.3/css/bootstrap.min.css" integrity="sha384-MCw98/SFnGE8fJT3GXwEOngsV7Zt27NXFoaoApmYm81iuXoPkFOJwJ8ERdknLPMO" crossorigin="anonymous">

	<link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.7.2/css/all.css" integrity="sha384-fnmOCqbTlWIlj8LyTjo7mOUStjsKC4pOpQbqyi7RrhN7udi9RwhKkMHpvLbHG9Sr" crossorigin="anonymous">

	<link rel="stylesheet" type="text/css" href="css/style.css">

	<script  src="https://code.jquery.com/jquery-3.3.1.min.js" integrity=
	"sha256-FgpCb/KJQlLNfOu91ta32o/NMZxltwRo8QtmkMRdAu8=" crossorigin="anonymous"></script>

	<script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.3/umd/popper.min.js" integrity="sha384-ZMP7rVo3mIykV+2+9J3UJ46jBk0WLaUAdn689aCwoqbBJiSnjAK/l8WvCWPIPm49" crossorigin="anonymous"></script>

	<script src="https://stackpath.bootstrapcdn.com/bootstrap/4.1.3/js/bootstrap.min.js" integrity="sha384-ChfqqxuZUCnJSK3+MXmPNIyE6ZbWh2IMqE241rYiqJxyMiZ6OW/JmZQ5stwEULTy" crossorigin="anonymous"></script>
</head>
<body>
	<div class="overlay">
		<div class="loader"></div>
	</div>
	<br>
	<br>

	<div class="container">

		<div class="card mx-auto" style="width: 24rem;">
			<p></p>
			<h5 class="card-title mx-auto">Register</h5>
		  <img class="card-img-top mx-auto" style="width: 30%;" src="images/add-icon-png-2487.png" alt="Login Icon">
		  <div class="card-body">
		    <form id="register_form" onsubmit="return false" autocomplete="off">
			  <div class="form-group">
			    <label for="username">Full name</label>
			    <input type="text" name="username" class="form-control" id="username" placeholder="Enter Username">
			    <small id="u_error" class="form-text text-muted"></small>
			  </div>
			  <div class="form-group">
			    <label for="email">Email address</label>
			    <input type="email" name="email" class="form-control" id="email" placeholder="Enter email">
			    <small id="e_error" class="form-text text-muted">We'll never share your email with anyone else.</small>
			  </div>
			  <div class="form-group">
			    <label for="password">Password</label>
			    <input type="password" name="password" class="form-control" id="password" placeholder="Password">
			    <small id="p_error" class="form-text text-muted"></small>
			  </div>
			  <div class="form-group">
			    <label for="confirm_password"> Confirm Password</label>
			    <input type="password" name="confirm_password" class="form-control" id="confirm_password" placeholder="Enter Password again">
			    <small id="cp_error" class="form-text text-muted"></small>
			  </div>
			  <div class="form-group">
			    <label for="usertype">Usertype</label>
			    <select name="usertype" id="usertype" class="form-control">
			    	<option value="">Choose User Type</option>
			    	<option value="1">Admin</option>
			    	<option value="0">Other</option>
			    </select>
			    <small id="t_error" class="form-text text-muted"></small>
			  </div>
			  <button type="submit" class="btn btn-primary"><i class="fas fa-user-plus">&nbsp;</i>Register</button>
			</form>
			<div class="container ml-0 p-0">
				<span><a href="login.php">Sign Into your Account</a></span>
			</div>
		  </div>
		  <div class="card-footer">
		  	<span></span>
		  </div>
		</div>

	</div><!--/container-->
	
</body>

<script type="text/javascript" src="js/main.js"></script>
</html>