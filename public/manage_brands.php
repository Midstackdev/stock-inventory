<?php
include_once "./database/constants.php";
if(!isset($_SESSION['userid'])){
	header("Location: ".DOMAIN."/login.php");
}

?>

<!DOCTYPE html>
<html lang="en">
<head>
	<meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
	<title>Inventory Management System</title>
	<link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.1.3/css/bootstrap.min.css" integrity="sha384-MCw98/SFnGE8fJT3GXwEOngsV7Zt27NXFoaoApmYm81iuXoPkFOJwJ8ERdknLPMO" crossorigin="anonymous">

	<link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.7.2/css/all.css" integrity="sha384-fnmOCqbTlWIlj8LyTjo7mOUStjsKC4pOpQbqyi7RrhN7udi9RwhKkMHpvLbHG9Sr" crossorigin="anonymous">

	<script  src="https://code.jquery.com/jquery-3.3.1.min.js" integrity=
	"sha256-FgpCb/KJQlLNfOu91ta32o/NMZxltwRo8QtmkMRdAu8=" crossorigin="anonymous"></script>

	<script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.3/umd/popper.min.js" integrity="sha384-ZMP7rVo3mIykV+2+9J3UJ46jBk0WLaUAdn689aCwoqbBJiSnjAK/l8WvCWPIPm49" crossorigin="anonymous"></script>

	<script src="https://stackpath.bootstrapcdn.com/bootstrap/4.1.3/js/bootstrap.min.js" integrity="sha384-ChfqqxuZUCnJSK3+MXmPNIyE6ZbWh2IMqE241rYiqJxyMiZ6OW/JmZQ5stwEULTy" crossorigin="anonymous"></script>
</head>
<body>

	<?php include "../templates/header.php"; ?>
	<br><br>

	<div class="container">
		<table class="table table-sm table-hover table-bordered">
		  <thead>
		    <tr>
		      <th scope="col">#</th>
		      <th scope="col">Brand</th>
		      <th scope="col">Status</th>
		      <th scope="col">Action</th>
		    </tr>
		  </thead>
		  <tbody id="get_brand">
		   <!--  <tr>
		      <th scope="row">1</th>
		      <td>Electronics</td>
		      <td>Root</td>
		      <td><a href="#" class="badge badge-pill badge-success">Success</a></td>
		      <td>
		      	<a href="#" class="btn btn-danger btn-sm">Delete</a>
		      	<a href="#" class="btn btn-info btn-sm">Edit</a>
		      </td>
		    </tr> -->
		    
		  </tbody>
		</table>
	</div><!--/container-->

	<!-- Button trigger modal -->

<!-- forms modals -->
<?php include '../templates/update_brand.php'; ?>

	
</body>

<!--script type="text/javascript" src="js/main.js"></script-->
<script type="text/javascript" src="js/manage.js"></script>
</html>