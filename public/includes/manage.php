<?php

/**
 * 
 */
class Manage 
{
	
	private $con;
	
	function __construct()
	{
		include_once "../database/db.php";
		$db = new Database();
		$this->con = $db->connect();
	}


	public function manageRecordWithPagination($table, $pno){
		$a = $this->pagination($this->con, $table, $pno, 10);
		if($table == "categories"){
			$sql = "SELECT p.cid AS cid, p.category_name AS category, c.category_name AS parent, p.status FROM categories p LEFT JOIN categories c ON p.parent_cat = c.cid ".$a["limit"];
		}else if($table == "products"){
			$sql = "SELECT p.pid, p.product_name, c.category_name, b.brand_name, p.product_price, p.product_stock, p.added_date, p.p_status FROM products p, brands b, categories c WHERE p.bid = b.bid AND p.cid = c.cid ".$a["limit"];
		}else{
			$sql = "SELECT * FROM ".$table." ".$a["limit"];
		}
		$result = $this->con->query($sql) or die($this->con->error);
		$rows = array();
		if($result->num_rows > 0){
			while($row = $result->fetch_assoc()){
				$rows[] = $row;
			}
		}
		return ["rows" =>$rows, "pagination" => $a["pagination"]];
	}


	private function pagination($con, $table, $pno, $n){
	$query = $con->query("SELECT COUNT(*) as rows FROM ".$table);
	$row = mysqli_fetch_assoc($query);
	// $totalRecords = 100000;
	$pageno = $pno;
	$numberOfRecordsPerPage = $n;

	$last = ceil($row['rows'] / $numberOfRecordsPerPage);


	$pagination = "<ul class='pagination'>";

	if($last != 1){
		if($pageno > 1){
			$previous = "";
			$previous = $pageno - 1;
			$pagination .= "<li class='page-item'><a class='page-link' pn='".$previous."' href='javascript:void(0);' style='color:#333;'> Previous </a></li>";
		}
		for ($i=$pageno - 5; $i < $pageno ; $i++) { 
			if($i > 0){
				$pagination .= "<li class='page-item'><a class='page-link' pn='".$i."' href='javascript:void(0);'> ".$i." </a></li>";
			}
			
		}
		$pagination .= "<li class='page-item'><a class='page-link' pn='".$pageno."' href='javascript:void(0);' style='color:#333;'>".$pageno."</a></li>";
		for ($i=$pageno + 1; $i <= $last ; $i++) { 
			$pagination .= "<li class='page-item'><a class='page-link' pn='".$i."' href='javascript:void(0);' > ".$i." </a></li>";
			if($i > $pageno + 4){
				break;
			}
		}
		if($last > $pageno){
			$next = $pageno + 1;
			$pagination .= "<li class='page-item'><a class='page-link' pn='".$next."' href='javascript:void(0);' style='color:#333;'>Next</a></li></ul>";
		}
	}

	// LIMIT 0,10
	$limit = "LIMIT ".($pageno - 1) * $numberOfRecordsPerPage."," .$numberOfRecordsPerPage;  

	return ["pagination" => $pagination, "limit" => $limit];
}

public function deleteRecord($table, $pk, $id){
	if($table == "categories"){
		$stmt = $this->con->prepare("SELECT ".$id." FROM categories WHERE parent_cat = ? ");
		$stmt->bind_param("i", $id);
		$stmt->execute();
		$result = $stmt->get_result() or die($this->con->error);
		if($result->num_rows > 0){
			return "DEPENDENT_CATEGORY";
		}else{
			$stmt = $this->con->prepare("DELETE FROM ".$table." WHERE ".$pk." = ?"); 
			$stmt->bind_param("i", $id);
			$result = $stmt->execute() or die($this->con->error);
			if($result){
				return "CATEGORY_DELETED";
			}
		}
	}else{
		$stmt = $this->con->prepare("DELETE FROM ".$table." WHERE ".$pk." = ?");
		$stmt->bind_param("i", $id);
		$result = $stmt->execute() or die($this->con->error);
		if($result){
			return "ORDER_DELETED";
		}
	}
}

public function getSingleRecord($table, $pk, $id){
	$stmt = $this->con->prepare("SELECT * FROM ".$table." WHERE ".$pk. " = ? LIMIT 1");
	$stmt->bind_param("i", $id);
	$stmt->execute() or die($this->con->error);
	$result = $stmt->get_result();
	if($result->num_rows == 1){
		$row = $result->fetch_assoc();
	}
	return $row;
}


public function updateRecord($table, $where, $fields){
	$sql = "";
	$condition = "";
	foreach ($where as $key => $value) {
		# id = '5' AND name = 'something'
		$condition .= $key . "='" .$value . "' AND ";
	}
	$condition = substr($condition, 0, -5);
	foreach ($fields as $key => $value) {
		# UPDATE table SET name = '', qty = '' WHERE id = '';
		$sql .= $key . "='" .$value. "', ";
	}
	$sql = substr($sql, 0, -2);
	$sql = "UPDATE ".$table." SET ".$sql." WHERE ".$condition;
	if($this->con->query($sql)){
		return "UPDATED"; 
	}
}

public function storeCustomerOrderInvoice($order_date, $cust_name, $ar_tqty, $ar_qty, $ar_price, $ar_pro_name, $sub_total, $gst, $discount, $net_total, $paid, $due, $payment_type){
	$stmt = $this->con->prepare("INSERT INTO `invoice`(`customer_name`, `order_date`, `sub_total`, `gst`, `discount`, `net_total`, `paid`, `due`, `payment_type`) VALUES (?,?,?,?,?,?,?,?,?)");
	$stmt->bind_param("ssdddddds", $cust_name, $order_date, $sub_total, $gst, $discount, $net_total, $paid, $due, $payment_type);
	$stmt->execute() or die($this->con->error);
	$invoice_no = $stmt->insert_id; 
	if($invoice_no != null){

		for ($i=0; $i < count($ar_price); $i++) { 

			// Find remaining quatity after giving customer and update
			$rem_qty = $ar_tqty[$i] - $ar_qty[$i];
			if($rem_qty < 0){
				return "ORDER FAIL_TO_COMPLETE";
			}else{
				$sql = "UPDATE products SET product_stock = '$rem_qty' WHERE product_name = '".$ar_pro_name[$i]."' ";
				$this->con->query($sql) or die($this->con-error);
			}

			$insert_product = $this->con->prepare("INSERT INTO `invoice_details`(`invoice_no`, `product_name`, `price`, `qty`) VALUES (?,?,?,?)");
			$insert_product->bind_param("isdd", $invoice_no, $ar_pro_name[$i], $ar_price[$i], $ar_qty[$i]);
			$insert_product->execute() or die($this->con->error);
		}

		return $invoice_no;
		
	}
	
}




}


// $obj = new Manage();
// echo "<pre>";
// print_r($obj->manageRecordWithPagination("categories", 1));
// echo $obj->deleteRecord("categories", "cid", 17);
// print_r($obj->getSIngleRecord("categories", "cid", 1));
// echo $obj->updateRecord("categories", ["cid"=>1], ["parent_cat"=>0, "category_name"=>"Electro", "status"=>1]); 





?>