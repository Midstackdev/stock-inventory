<?php

/**
 * 

 */
class DBOperation
{	
	private $con;
	
	function __construct()
	{
		include_once "../database/db.php";
		$db = new Database();
		$this->con = $db->connect();
	}

	public function addCategory($parent, $cat){
		$stmt = $this->con->prepare("INSERT INTO `categories`(`parent_cat`, `category_name`, `status`) VALUES (?,?,?)");
		$status = 1;
		$stmt->bind_param("isi", $parent, $cat, $status);
		$result = $stmt->execute() or die($this->con->error);
		if($result){
			return "CATEGORY_ADDED";
		}else{
			return 0;
		}
	}

	public function addBrand($brand_name){
		$stmt = $this->con->prepare("INSERT INTO `brands`(`brand_name`, `status`) VALUES (?,?)");
		$status = 1;
		$stmt->bind_param("si", $brand_name, $status);
		$result = $stmt->execute() or die($this->con->error);
		if($result){
			return "BRAND_ADDED";
		}else{
			return 0;
		}
	}

	public function addProduct($cid, $bid, $pro_name, $price, $stock, $date){
		$stmt = $this->con->prepare("INSERT INTO `products`(`cid`, `bid`, `product_name`, `product_price`, `product_stock`, `added_date`, `p_status`) VALUES (?,?,?,?,?,?,?)");
		$status = 1;
		$stmt->bind_param("iisdisi", $cid, $bid, $pro_name, $price, $stock, $date, $status);
		$result = $stmt->execute() or die($this->con->error);
		if($result){
			return "NEW_PRODUCT_ADDED";
		}else{
			return 0;
		}
	}

	public function getAllRecord($table){
		$stmt = $this->con->prepare("SELECT * FROM ".$table);
		$stmt->execute() or die($this->con->error);
		$result = $stmt->get_result();
		$rows = array();
		if($result->num_rows > 0){
			while($row = $result->fetch_assoc()){
				$rows[] = $row;
			}
			return $rows;
		}
		return "NO_DATA";
	}





}

// $opr = new DBOperation();
// // echo $opr->addCategory(1, "Mobile");
// echo "<pre>";
// print_r($opr->getAllRecord("categories"));

/*CREATE TABLE products(
pid INT(11) NOT NULL AUTO_INCREMENT,
cid INT(11) NOT NULL,
bid INT(11) NOT NULL,
    product_name VARCHAR(100) NOT NULL,
    product_price DOUBLE NOT NULL,
    product_stock INT(11) NOT NULL,
    added_date DATE NOT NULL,
    p_status ENUM('1', '0') NOT NULL,
    PRIMARY KEY(pid),
    UNIQUE KEY(product_name),
    FOREIGN KEY(cid) REFERENCES categories (cid),
    FOREIGN KEY(bid) REFERENCES brands(bid)
);

SELECT 
p.category_name as category,
c.category_name as parent,
p.status 
FROM categories p
LEFT JOIN categories c ON 
p.parent_cat = c.cid


CREATE TABLE invoice(
    invoice_no INT(11) PRIMARY KEY AUTO_INCREMENT,
    customer_name VARCHAR(100) NOT NULL,
    order_date DATE NOT NULL,
    sub_total DOUBLE NOT NULL,
    gst DOUBLE NOT NULL,
    discount DOUBLE NOT NULL,
    net_total DOUBLE NOT NULL,
    paid DOUBLE NOT NULL,
    due DOUBLE NOT NULL,
    payment_type TEXT(20) NOT NULL

);

CREATE TABLE invoice_details(
	id INT(11) PRIMARY KEY AUTO_INCREMENT,
    invoice_no INT(11) NOT NULL,
    product_name VARCHAR(100) NOT NULL,
    price DOUBLE NOT NULL,
    qty INT(11) NOT NULL,
    FOREIGN KEY (invoice_no) REFERENCES invoice (invoice_no)
);



*/




?>