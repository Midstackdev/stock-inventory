<?php
/**
 * 
 */
class User
{
	private $con;

	function __construct()
	{
		include_once "../database/db.php";
		$db = new Database();
		$this->con = $db->connect();
		// if($this->con){
		// 	echo "Conneted";
		// }
	}

	// User already registered or not
	private function emailExists($email){
		$stmt = $this->con->prepare("SELECT id FROM users WHERE email = ?");
		$stmt->bind_param("s", $email);
		$stmt->execute() or die($this->con->error);
		$result = $stmt->get_result();
		if($result->num_rows > 0){
			return 1;
		}else{
			return 0;
		}
	}


	public function createUserAccount($username, $email, $password, $usertype){

		if($this->emailExists($email)){
			return "EMAIL_ALREADY_EXISTS";
		}else{
			$password = password_hash($password, PASSWORD_BCRYPT, ["cost" => 8]);
			$date = date("Y-m-d H:i:s");
			$notes = ""; 
			$stmt = $this->con->prepare("INSERT INTO `users`(`username`, `email`, `password`, `usertype`, `register_date`, `last_login`, `notes`) VALUES (?,?,?,?,?,?,?)");
			$stmt->bind_param("sssssss", $username,$email,$password,$usertype,$date,$date,$notes);
			$result = $stmt->execute() or die($this->con->error);
			if($result){
				return $this->con->insert_id;
			}else{
				return "SOME_ERROR";
			}
		}
		

	}

	public function userLogin($email, $password){
		$stmt = $this->con->prepare("SELECT id, username, password, last_login FROM users WHERE email = ?");
		$stmt->bind_param("s", $email);
		$stmt->execute() or die($this->con->error);
		$result = $stmt->get_result();

		if($result->num_rows < 1){
			return "NOT REGISTERED";
		}else{
			$row = $result->fetch_assoc();
			if(password_verify($password, $row["password"])){
				$_SESSION["userid"] = $row["id"];
				$_SESSION["username"] = $row["username"];
				$_SESSION["last_login"] = $row["last_login"];

				// updating user last login time
				$last_login = date("Y-m-d H:i:s");
				$stmt = $this->con->prepare("UPDATE users SET last_login = ? WHERE email = ?");
				$stmt->bind_param("ss", $last_login, $email);
				$result = $stmt->execute() or die($this->con->error);
				if($result){
					return 1;
				}else{
					return 0;
				}
			}else{
				return "PASSWORD_NOT_MATCHED";
			}
		}
	}





}

// $user = new User();
// // echo $user->createUserAccount("Admino", "admin@admin.com", "password", "Admin");
// echo $user->userLogin("admin@admin.com", "password");
// echo $_SESSION["username"];




?>