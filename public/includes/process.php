<?php
include_once '../database/constants.php';
include_once 'user.php';
include_once 'DBOperation.php';
include_once 'manage.php';

// registration Process
if(isset($_POST["username"]) && isset($_POST["email"])){
	$user = new User();
	$result = $user->createUserAccount($_POST["username"], $_POST["email"], $_POST["password"], $_POST["usertype"]);
	echo $result;
	exit();
}


// Login Processing
if(isset($_POST["email"]) && isset($_POST["password"])){
	$user = new User();
	$result = $user->userLogin($_POST["email"], $_POST["password"]);
	echo $result;
	exit();
}

// TO get Category
if(isset($_POST['getCategory'])){
	$obj = new DBOperation();
	$rows = $obj->getAllRecord("categories");
	foreach ($rows as $row) {
		echo "<option value='".$row["cid"]."'>".$row["category_name"]."</option>";
	}
	exit();
}

// TO get Brand
if(isset($_POST['getBrand'])){
	$obj = new DBOperation();
	$rows = $obj->getAllRecord("brands");
	foreach ($rows as $row) {
		echo "<option value='".$row["bid"]."'>".$row["brand_name"]."</option>";
	}
	exit();
}

// Add Category
if(isset($_POST["category_name"]) && isset($_POST["parent_cat"])){
	$obj = new DBOperation();
	$result = $obj->addCategory($_POST["parent_cat"], $_POST["category_name"]);
	echo $result;
	exit();
}

// Add Brand
if(isset($_POST['brand_name'])){
	$obj = new DBOperation();
	$result = $obj->addBrand($_POST['brand_name']);
	echo $result;
	exit();
}

// Add Product
if(isset($_POST['added_date']) && isset($_POST['product_name'])){
	$obj = new DBOperation();
	$result = $obj->addProduct($_POST['select_cat'], $_POST['select_brand'], $_POST['product_name'], $_POST['product_price'], 
		$_POST['product_qty'], $_POST['added_date']);
	echo $result;
	exit();
}

// Manage Category
if(isset($_POST['manageCategory'])){
	$m = new Manage();
	$result = $m->manageRecordWithPagination("categories", $_POST['pageno']);
	$rows = $result["rows"];
	$pagination = $result["pagination"];
	if(count($rows) > 0){
		$n = (($_POST['pageno'] -1) * 10) + 1;
		foreach($rows as $row){
			?>
			<tr>
		      <th scope="row"><?php echo $n; ?></th>
		      <td><?php echo $row['category']; ?></td>
		      <td><?php echo $row['parent']; ?></td>
		      <td><a href="#" class="badge badge-pill badge-success">Active</a></td>
		      <td>
		      	<a href="javascript:void(0);" did="<?php echo $row['cid']; ?>" class="btn btn-danger btn-sm del_cat">Delete</a>
		      	<a href="javascript:void(0);" eid="<?php echo $row['cid']; ?>" class="btn btn-info btn-sm edit_cat">Edit</a>
		      </td>
		    </tr>


			<?php
			$n++;
		}
		?>
			<tr><td colspan="5"><?php echo $pagination; ?></td></tr>

		<?php
		exit();
	}
}

// DELETE Category
if(isset($_POST['deleteCategory'])){
	$m = new Manage();
	$result = $m->deleteRecord("categories", "cid", $_POST['id']);
	echo $result;
}

// Get Single category record
if(isset($_POST['updateCategory'])){
	$m = new Manage();
	$result = $m->getSingleRecord("categories", "cid", $_POST['id']);
	echo json_encode($result);
	exit();
}

// Update category Record 
if(isset($_POST['update_category'])){
	$m = new Manage();
	$id = $_POST['cid'];
	$name = $_POST['update_category'];
	$parent_cat = $_POST['parent_cat'];
	$result = $m->updateRecord("categories", ["cid"=>$id], ["parent_cat"=>$parent_cat, "category_name"=>$name, "status"=>1]); 
	echo $result;
}



// ---------------------------Brands-------------------------------------------------------------
// Manage Brand
if(isset($_POST['manageBrand'])){
	$m = new Manage();
	$result = $m->manageRecordWithPagination("brands", $_POST['pageno']);
	$rows = $result["rows"];
	$pagination = $result["pagination"];
	if(count($rows) > 0){
		$n = (($_POST['pageno'] -1) * 10) + 1;
		foreach($rows as $row){
			?>
			<tr>
		      <th scope="row"><?php echo $n; ?></th>
		      <td><?php echo $row['brand_name']; ?></td>
		      <td><a href="#" class="badge badge-pill badge-success">Active</a></td>
		      <td>
		      	<a href="javascript:void(0);" did="<?php echo $row['bid']; ?>" class="btn btn-danger btn-sm del_brand">Delete</a>
		      	<a href="javascript:void(0);" eid="<?php echo $row['bid']; ?>" data-toggle="modal" data-target="#form_brand_update" class="btn btn-info btn-sm edit_brand">Edit</a>
		      </td>
		    </tr>


			<?php
			$n++;
		}
		?>
			<tr><td colspan="5"><?php echo $pagination; ?></td></tr>

		<?php
		exit();
	}
}

// DELETE Brand
if(isset($_POST['deleteBrand'])){
	$m = new Manage();
	$result = $m->deleteRecord("brands", "bid", $_POST['id']);
	echo $result;
}

// Get Single brand record
if(isset($_POST['updateBrand'])){
	$m = new Manage();
	$result = $m->getSingleRecord("brands", "bid", $_POST['id']);
	echo json_encode($result);
	exit();
}

// Update brand Record 
if(isset($_POST['update_brand'])){
	$m = new Manage();
	$id = $_POST['bid'];
	$name = $_POST['update_brand'];
	$result = $m->updateRecord("brands", ["bid"=>$id], ["brand_name"=>$name, "status"=>1]); 
	echo $result;
}


// -------------------------------Products----------------------------------------------

// Manage Product
if(isset($_POST['manageProduct'])){
	$m = new Manage();
	$result = $m->manageRecordWithPagination("products", $_POST['pageno']);
	$rows = $result["rows"];
	$pagination = $result["pagination"];
	if(count($rows) > 0){
		$n = (($_POST['pageno'] -1) * 10) + 1;
		foreach($rows as $row){
			?>
			<tr>
		      <th scope="row"><?php echo $n; ?></th>
		      <td><?php echo $row['product_name']; ?></td>
		      <td><?php echo $row['category_name']; ?></td>
		      <td><?php echo $row['brand_name']; ?></td>
		      <td><?php echo $row['product_price']; ?></td>
		      <td><?php echo $row['product_stock']; ?></td>
		      <td><?php echo $row['added_date']; ?></td>
		      <!-- <td><?php //echo $row['p_status']; ?></td> -->
		      <td><a href="#" class="badge badge-pill badge-success">Active</a></td>
		      <td>
		      	<a href="javascript:void(0);" did="<?php echo $row['pid']; ?>" class="btn btn-danger btn-sm del_product">Delete</a>
		      	<a href="javascript:void(0);" eid="<?php echo $row['pid']; ?>" data-toggle="modal" data-target="#form_product_update" class="btn btn-info btn-sm edit_product">Edit</a>
		      </td>
		    </tr>


			<?php
			$n++;
		}
		?>
			<tr><td colspan="9"><?php echo $pagination; ?></td></tr>

		<?php
		exit();
	}
}

// DELETE Product
if(isset($_POST['deleteProduct'])){
	$m = new Manage();
	$result = $m->deleteRecord("products", "pid", $_POST['id']);
	echo $result;
}

// Get Singleproduct record
if(isset($_POST['updateProduct'])){
	$m = new Manage();
	$result = $m->getSingleRecord("products", "pid", $_POST['id']);
	echo json_encode($result);
	exit();
}

// Update product Record 
if(isset($_POST['update_product'])){
	$m = new Manage();
	$id = $_POST['pid'];
	$name = $_POST['update_product'];
	$cat = $_POST['select_cat']; 
	$brand = $_POST['select_brand']; 
	$price = $_POST['product_price']; 
	$qty = $_POST['product_qty']; 
	$date = $_POST['added_date'];
	$result = $m->updateRecord("products", ["pid"=>$id], ["cid"=>$cat, "bid"=>$brand, "product_name"=>$name, "product_price"=>$price, "product_stock"=>$qty, "added_date"=>$date, "p_status"=>1]); 
	echo $result;
}

// --------------------------------------------------Order Processing--------------------------------------------------

if(isset($_POST['getNewOrderItem'])){
	$obj = new DBOperation();
	$rows = $obj->getAllRecord("products");
	?>
	<tr>
		<td><b class="number">1</b></td>
		<td>
			<select name="pid[]" class="form-control form-control-sm pid" required>
				<option value="">Choose Product</option>
				<?php 
				foreach($rows as $row){
					?>
					<option value="<?php echo $row['pid'] ?>"><?php echo $row['product_name'] ?></option>
					<?php
				}
				?>
			</select>
			<small class="form-text text-muted p-error mt-0"></small>
		</td>
		<td><input name="tqty[]" type="text" class="form-control form-control-sm tqty" readonly></td>
		<td><input name="qty[]" type="text" class="form-control form-control-sm qty" required></td>
		<td><input name="price[]" type="text" class="form-control form-control-sm price" readonly>
		<span><input name="pro_name[]" type="hidden" class="form-control form-control-sm pro_name" readonly></span></td>
		<td>GHs.<span class="amt">0</span></td>
	</tr>

	<?php
}

// Get price and qty of one item
if(isset($_POST['getPriceAndQty'])){
	$m = new Manage();
	$result = $m->getSingleRecord("products", "pid", $_POST["id"]);
	echo json_encode($result);
	exit();
}


if(isset($_POST["order_date"]) && isset($_POST["cust_name"])){
	$order_date = $_POST["order_date"];
	$cust_name = $_POST["cust_name"];

	// Getting array from order_form
	$ar_tqty = $_POST["tqty"];
	$ar_qty = $_POST["qty"];
	$ar_price = $_POST["price"];
	$ar_pro_name = $_POST["pro_name"];

	$sub_total = $_POST["sub_total"];
	$gst = $_POST["gst"];
	$discount = $_POST["discount"];
	$net_total = $_POST["net_total"];
	$paid = $_POST["paid"];
	$due = $_POST["due"];
	$payment_type = $_POST["payment_type"];

	$m = new Manage();
	echo $result = $m->storeCustomerOrderInvoice($order_date, $cust_name, $ar_tqty, $ar_qty, $ar_price, $ar_pro_name, $sub_total, $gst, $discount, $net_total, $paid, $due, $payment_type);



}


// -------------------------------Invoices----------------------------------------------

// Manage Product
if(isset($_POST['manageInvoice'])){
	$m = new Manage();
	$result = $m->manageRecordWithPagination("invoice", $_POST['pageno']);
	$rows = $result["rows"];
	$pagination = $result["pagination"];
	if(count($rows) > 0){
		$n = (($_POST['pageno'] -1) * 10) + 1;
		foreach($rows as $row){
			?>
			<tr>
		      <th scope="row"><?php echo $n; ?></th>
		      <td><?php echo $row['customer_name']; ?></td>
		      <td><?php echo $row['order_date']; ?></td>
		      <td><?php echo $row['invoice_no']; ?></td>
		      <td><?php echo $row['paid']; ?></td>
		      <td><?php echo $row['due']; ?></td>
		      <td><?php echo $row['payment_type']; ?></td>
		      <!-- <td><?php //echo $row['p_status']; ?></td> -->
		      <td><a href="#" class="badge badge-pill badge-success">Active</a></td>
		      <td>
		      	<a href="javascript:void(0);" did="<?php echo $row['invoice_no']; ?>" class="btn btn-danger btn-sm del_product">Delete</a>
		      	<a href="javascript:void(0);" eid="<?php echo $row['invoice_no']; ?>" data-toggle="modal" data-target="#form_product_update" class="btn btn-info btn-sm edit_product">Print</a>
		      </td>
		    </tr>


			<?php
			$n++;
		}
		?>
			<tr><td colspan="9"><?php echo $pagination; ?></td></tr>

		<?php
		exit();
	}
}


?>