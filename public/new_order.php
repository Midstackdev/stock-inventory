<?php
include_once "./database/constants.php";
if(!isset($_SESSION['userid'])){
	header("Location: ".DOMAIN."/login.php");
}

?>

<!DOCTYPE html>
<html lang="en">
<head>
	<meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
	<title>Inventory Management System</title>
	<link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.1.3/css/bootstrap.min.css" integrity="sha384-MCw98/SFnGE8fJT3GXwEOngsV7Zt27NXFoaoApmYm81iuXoPkFOJwJ8ERdknLPMO" crossorigin="anonymous">

	<link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.7.2/css/all.css" integrity="sha384-fnmOCqbTlWIlj8LyTjo7mOUStjsKC4pOpQbqyi7RrhN7udi9RwhKkMHpvLbHG9Sr" crossorigin="anonymous">

	<script  src="https://code.jquery.com/jquery-3.3.1.min.js" integrity=
	"sha256-FgpCb/KJQlLNfOu91ta32o/NMZxltwRo8QtmkMRdAu8=" crossorigin="anonymous"></script>

	<script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.3/umd/popper.min.js" integrity="sha384-ZMP7rVo3mIykV+2+9J3UJ46jBk0WLaUAdn689aCwoqbBJiSnjAK/l8WvCWPIPm49" crossorigin="anonymous"></script>

	<script src="https://stackpath.bootstrapcdn.com/bootstrap/4.1.3/js/bootstrap.min.js" integrity="sha384-ChfqqxuZUCnJSK3+MXmPNIyE6ZbWh2IMqE241rYiqJxyMiZ6OW/JmZQ5stwEULTy" crossorigin="anonymous"></script>
</head>
<body>
	<div class="overlay">
		<div class="loader"></div>
	</div>

	<?php include "../templates/header.php"; ?>
	<br><br>

	<div class="container">
		<div class="row">
			<div class="col-md-10 mx-auto">
				<div class="card" style="box-shadow: 0 0 25px 0 lightgrey;">
				  <div class="card-header">
				    <h4>New Orders</h4>
				  </div>
				  <div class="card-body">
				    <form id="get_order_data" onsubmit="return false">
				    	<div class="form-group row">
				    		<label class="col-sm-3" align="right">Order Date</label>
				    		<div class="col-sm-6">
				    			<input type="text" id="order_date" name="order_date" class="form-control form-control-sm" value="<?php echo date("Y-m-d"); ?>" readonly>
				    		</div>
				    	</div>

				    	<div class="form-group row">
				    		<label class="col-sm-3" align="right">Customer Name*</label>
				    		<div class="col-sm-6">
				    			<input type="text" id="cust_name" name="cust_name" class="form-control form-control-sm" placeholder="Enter Customer Name" required>
				    			<small id="n_error" class="form-text text-muted"></small>
				    		</div>
				    	</div>

				    	<div class="card" style="box-shadow: 0 0 15px 0 lightgrey;">
				    		<div class="card-body">
				    			<h3>Make a order list</h3>
				    			<table align="center" style="width: 800px;">
				    				<thead>
				    					<tr>
				    						<th>#</th>
				    						<th style="text-align:center;">Item Name</th>
				    						<th style="text-align:center;">Total Quantity</th>
				    						<th style="text-align:center;">Quantity</th>
				    						<th style="text-align:center;">Price</th>
											<!-- <th style="text-align:center;"></th> -->
											<th>Total</th>	
				    					</tr>
				    				</thead>
				    				<tbody id="invoice_item">
				    					<!-- <tr>
				    						<td><b id="number">1</b></td>
				    						<td>
				    							<select name="pid[]" class="form-control form-control-sm" required>
				    								<option>Washing Machine</option>
				    							</select>
				    						</td>
				    						<td><input name="tqty[]" type="text" class="form-control form-control-sm" readonly></td>
				    						<td><input name="qty[]" type="text" class="form-control form-control-sm" required></td>
				    						<td><input name="price[]" type="text" class="form-control form-control-sm" readonly></td>
				    						<td>GHC 1500</td>
				    					</tr> -->
				    				</tbody>
				    				
				    			</table>
				    				<center style="margin: 10px;">
				    					<button id="add" class="btn btn-success" style="width: 150px;">Add</button>
				    					<button id="remove" class="btn btn-danger" style="width: 150px;">Remove</button>
				    				</center>
				    		</div><!--/card body-->
				    	</div><!--/card-->

				    	<br><br>
				    	<div class="form-group row">
				    		<label class="col-sm-3" align="right">Sub Total</label>
				    		<div class="col-sm-6">
				    			<input type="text" name="sub_total" id="sub_total" class="form-control form-control-sm" required readonly>
				    		</div>
				    	</div>

				    	<div class="form-group row">
				    		<label class="col-sm-3" align="right">GST (17%)</label>
				    		<div class="col-sm-6">
				    			<input type="text" name="gst" id="gst" class="form-control form-control-sm" required readonly>
				    		</div>
				    	</div>

				    	<div class="form-group row">
				    		<label class="col-sm-3" align="right">Discount</label>
				    		<div class="col-sm-6">
				    			<input type="text" name="discount" id="discount" class="form-control form-control-sm" required>
				    			<small id="d_error" class="form-text text-muted"></small>
				    		</div>
				    	</div>

				    	<div class="form-group row">
				    		<label class="col-sm-3" align="right">Net Total</label>
				    		<div class="col-sm-6">
				    			<input type="text" name="net_total" id="net_total" class="form-control form-control-sm" required readonly>
				    		</div>
				    	</div>

				    	<div class="form-group row">
				    		<label class="col-sm-3" align="right">Paid</label>
				    		<div class="col-sm-6">
				    			<input type="text" name="paid" id="paid" class="form-control form-control-sm" required>
				    			<small id="p_error" class="form-text text-muted"></small>
				    		</div>
				    	</div>

				    	<div class="form-group row">
				    		<label class="col-sm-3" align="right">Due</label>
				    		<div class="col-sm-6">
				    			<input type="text" name="due" id="due" class="form-control form-control-sm" required readonly>
				    		</div>
				    	</div>

				    	<div class="form-group row">
				    		<label class="col-sm-3" align="right">Payment Method</label>
				    		<div class="col-sm-6">
				    			<select type="text" name="payment_type" id="payment_type" class="form-control form-control-sm" required>
									<option>Cash</option>
									<option>Card</option>
									<option>Draft</option>
									<option>Cheque</option>
				    			</select>
				    			<small id="pm_error" class="form-text text-muted"></small>	
				    		</div>
				    	</div>
						
						<center>
							<input type="submit" id="order_form" style="width: 150px;" class="btn btn-info" value="Order">
							<input type="submit" id="print_invoice" style="width: 150px;" class="btn btn-success d-none" value="Print Invoice">
						</center>	

				    </form>
				  </div>
				</div><!--/card-->

			</div><!--/col-10-->

			
		</div><!--/row-->

	
	</div><!--/container-->




</body>
<!-- 
<script type="text/javascript" src="js/main.js"></script>
 --><script type="text/javascript" src="js/order.js"></script>
</html>