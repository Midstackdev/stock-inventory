<?php
include_once "./database/constants.php";
if(!isset($_SESSION['userid'])){
	header("Location: ".DOMAIN."/login.php");
}

?>

<!DOCTYPE html>
<html lang="en">
<head>
	<meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
	<title>Inventory Management System</title>
	<link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.1.3/css/bootstrap.min.css" integrity="sha384-MCw98/SFnGE8fJT3GXwEOngsV7Zt27NXFoaoApmYm81iuXoPkFOJwJ8ERdknLPMO" crossorigin="anonymous">

	<link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.7.2/css/all.css" integrity="sha384-fnmOCqbTlWIlj8LyTjo7mOUStjsKC4pOpQbqyi7RrhN7udi9RwhKkMHpvLbHG9Sr" crossorigin="anonymous">

	<script  src="https://code.jquery.com/jquery-3.3.1.min.js" integrity=
	"sha256-FgpCb/KJQlLNfOu91ta32o/NMZxltwRo8QtmkMRdAu8=" crossorigin="anonymous"></script>

	<script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.3/umd/popper.min.js" integrity="sha384-ZMP7rVo3mIykV+2+9J3UJ46jBk0WLaUAdn689aCwoqbBJiSnjAK/l8WvCWPIPm49" crossorigin="anonymous"></script>

	<script src="https://stackpath.bootstrapcdn.com/bootstrap/4.1.3/js/bootstrap.min.js" integrity="sha384-ChfqqxuZUCnJSK3+MXmPNIyE6ZbWh2IMqE241rYiqJxyMiZ6OW/JmZQ5stwEULTy" crossorigin="anonymous"></script>
</head>
<body>

	<?php include "../templates/header.php"; ?>
	<br><br>

	<div class="container">
		<div class="row">
			<div class="col-md-4">
				<div class="card mx-auto" style="width: 20rem;">
				  <img class="card-img-top mx-auto" style="width: 60%" src="images/login-icon-3048.png" alt="User Image">
				  <div class="card-body">
				    <h5 class="card-title">Profile Info</h5>
				    <p class="card-text"><i class="fas fa-user">&nbsp;</i>Admino Bosso</p>
				    <p class="card-text"><i class="fas fa-user">&nbsp;</i>Admin</p>
				    <p class="card-text"><i class="fas fa-clock">&nbsp;</i>Last Login: xxxx-xx-xx</p>
				    <a href="#" class="btn btn-primary"><i class="fas fa-user-edit">&nbsp;</i>Edit Profile</a>
				  </div>
				</div>
			</div>	<!--/col-4-->

			<div class="col-md-8">
				<div class="jumbotron" style="width: 100%; height: 100%;">
					<h1>Welcome Admin, </h1>
					<div class="row">
						<div class="col-sm-6">
							<iframe src="http://free.timeanddate.com/clock/i6okqrly/n4/szw160/szh160/cf100/hnce1ead6" frameborder="0" width="160" height="160"></iframe>
						</div>

						<div class="col-sm-6">
							<div class="card">
						      <div class="card-body">
						        <h5 class="card-title">New Orders</h5>
						        <p class="card-text">Make Invoices and create new orders.</p>
						        <a href="new_order.php" class="btn btn-primary"><i class="fas fa-cart-plus">&nbsp;</i>New Orders</a>
						      </div>
						    </div>
						</div>
					</div>
					<div class="row">
						<div class="col-sm-6 offset-6">
							<div class="card">
						      <div class="card-body">
						        <h5 class="card-title">Mange Invoices</h5>
						        <!-- <p class="card-text">Make Invoices and create new orders.</p> -->
						        <a href="manage_invoice.php" class="btn btn-primary"><i class="fas fa-cart-plus">&nbsp;</i>Manage Invoices</a>
						      </div>
						    </div>
						</div>
					</div>

				</div>
			</div><!--/col-8-->
		</div><!--/row-->
	</div><!--/container-->
	<p></p>
	<p></p>

	<div class="container">
		<div class="row">
			<div class="col-md-4" >
				<div class="card mx-auto" style="width: 20rem;">
			      <div class="card-body">
			        <h5 class="card-title">Categories</h5>
			        <p class="card-text">Manage categories add parent and sub categories.</p>
			        <a href="#" class="btn btn-primary" data-toggle="modal" data-target="#form_category"><i class="fas fa-clipboard-list">&nbsp;</i>Add</a>
			        <a href="manage_categories.php" class="btn btn-primary"><i class="fas fa-clipboard-list">&nbsp;</i>Manage</a>
			      </div>
			    </div>
			</div>

			<div class="col-md-4">
				<div class="card">
			      <div class="card-body">
			        <h5 class="card-title">Brands</h5>
			        <p class="card-text">Manage brands and add new brands.</p>
			        <a href="#" class="btn btn-primary" data-toggle="modal" data-target="#form_brand"><i class="fab fa-btc">&nbsp;</i>Add</a>
			        <a href="manage_brands.php" class="btn btn-primary"><i class="fab fa-btc">&nbsp;</i>Manage</a>
			      </div>
			    </div>
			</div>

			<div class="col-md-4">
				<div class="card">
			      <div class="card-body">
			        <h5 class="card-title">Products</h5>
			        <p class="card-text">Manage products and add new products.</p>
			        <a href="#" class="btn btn-primary" data-toggle="modal" data-target="#form_product"><i class="fas fa-ruble-sign">&nbsp;</i>Add</a>
			        <a href="manage_products.php" class="btn btn-primary"><i class="fas fa-ruble-sign">&nbsp;</i>Manage</a>
			      </div>
			    </div>
			</div>

		</div><!--/row-->
	</div><!--/container-->

	<!-- Button trigger modal -->

<!-- forms modals -->
<?php include '../templates/category.php'; ?>

<?php include '../templates/brand.php'; ?>

<?php include '../templates/product.php'; ?>
	
</body>

<script type="text/javascript" src="js/main.js"></script>
</html>