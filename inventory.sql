-- phpMyAdmin SQL Dump
-- version 4.7.7
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Generation Time: Jul 18, 2019 at 09:03 PM
-- Server version: 10.1.30-MariaDB
-- PHP Version: 7.2.2

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `inventory`
--

-- --------------------------------------------------------

--
-- Table structure for table `brands`
--

CREATE TABLE `brands` (
  `bid` int(11) NOT NULL,
  `brand_name` varchar(255) NOT NULL,
  `status` enum('1','0','','') NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `brands`
--

INSERT INTO `brands` (`bid`, `brand_name`, `status`) VALUES
(1, 'Samsung', '1'),
(3, 'Hp', '1'),
(4, 'Apple', '1'),
(5, 'Adobe', '1'),
(7, 'Huawei', '1'),
(11, 'compaq', '1'),
(12, 'Toshiba', '1'),
(13, 'Lenovo', '1'),
(15, 'Teslas', '1'),
(19, 'Avex ', '1');

-- --------------------------------------------------------

--
-- Table structure for table `categories`
--

CREATE TABLE `categories` (
  `cid` int(11) NOT NULL,
  `parent_cat` int(11) NOT NULL,
  `category_name` varchar(255) NOT NULL,
  `status` enum('1','0','','') NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `categories`
--

INSERT INTO `categories` (`cid`, `parent_cat`, `category_name`, `status`) VALUES
(1, 0, 'Electronics', '1'),
(2, 0, 'Software', '1'),
(4, 0, 'Gadgets', '1'),
(6, 1, 'Mobile', '1'),
(10, 2, 'Antivirus', '1'),
(11, 2, 'Adobe Suite', '1'),
(12, 4, 'Laptops', '1'),
(17, 0, 'Homeware', '1'),
(18, 0, 'Kitchen', '1'),
(19, 0, 'Bathroom', '1'),
(20, 0, 'Playground', '1');

-- --------------------------------------------------------

--
-- Table structure for table `invoice`
--

CREATE TABLE `invoice` (
  `invoice_no` int(11) NOT NULL,
  `customer_name` varchar(100) NOT NULL,
  `order_date` date NOT NULL,
  `sub_total` double NOT NULL,
  `gst` double NOT NULL,
  `discount` double NOT NULL,
  `net_total` double NOT NULL,
  `paid` double NOT NULL,
  `due` double NOT NULL,
  `payment_type` tinytext NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `invoice`
--

INSERT INTO `invoice` (`invoice_no`, `customer_name`, `order_date`, `sub_total`, `gst`, `discount`, `net_total`, `paid`, `due`, `payment_type`) VALUES
(1, 'Han solo', '2019-03-26', 1200, 204, 0, 1404, 1404, 0, 'Cash'),
(2, 'Slow John', '2019-03-26', 2000, 340, 0, 2340, 2340, 0, 'Cash'),
(3, 'Bobby Fisher', '2019-03-26', 10000, 1700, 0, 11700, 11700, 0, 'Cash'),
(4, 'John Doe', '2019-03-26', 4000, 680, 0, 4680, 4680, 0, 'Cash'),
(5, 'Yamo Souq', '2019-03-26', 2200, 374, 0, 2574, 2574, 0, 'Cash'),
(6, 'bobby Fisher', '2019-03-26', 2000, 340, 0, 2340, 2340, 0, 'Cash'),
(7, 'Alima Snow', '2019-03-26', 1200, 204, 0, 1404, 1404, 0, 'Cash'),
(8, '', '2019-03-27', 1000, 170, 0, 1170, 0, 1170, 'Cash'),
(9, '', '2019-03-27', 1000, 170, 0, 1170, 0, 1170, 'Cash'),
(10, '', '2019-03-27', 1000, 170, 0, 1170, 0, 1170, 'Cash'),
(11, 'Hansolo', '2019-03-27', 1200, 204, 0, 1404, 1404, 0, 'Cash'),
(12, 'Bill Alfonso', '2019-03-27', 1980, 336, 0, 2316, 2316, 0, 'Cash'),
(13, 'Hall Jones', '2019-03-27', 5040, 856, 0, 5896, 5896, 0, 'Cash'),
(14, 'Costy Belly', '2019-03-27', 1100, 187, 0, 1287, 1287, 0, 'Cash'),
(15, 'bobby Fisher', '2019-03-27', 1300, 221, 0, 1521, 1521, 0, 'Cash'),
(16, 'Emma Smith', '2019-03-27', 4400, 748, 0, 5148, 5148, 0, 'Cash'),
(17, 'Han solo', '2019-04-09', 5100, 867, 0, 5967, 5967, 0, 'Cash');

-- --------------------------------------------------------

--
-- Table structure for table `invoice_details`
--

CREATE TABLE `invoice_details` (
  `id` int(11) NOT NULL,
  `invoice_no` int(11) NOT NULL,
  `product_name` varchar(100) NOT NULL,
  `price` double NOT NULL,
  `qty` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `invoice_details`
--

INSERT INTO `invoice_details` (`id`, `invoice_no`, `product_name`, `price`, `qty`) VALUES
(1, 3, 'Washing Nachine', 2000, 5),
(2, 4, 'Washing Machine', 2000, 1),
(3, 4, 'Samsung Galaxy S8', 2000, 1),
(4, 5, 'Pavilion 2000', 1200, 1),
(5, 5, 'iphone SE', 1000, 1),
(6, 6, 'Samsung Galaxy S8', 2000, 1),
(7, 7, 'Pavilion 2000', 1200, 1),
(8, 8, 'iphone SE', 1000, 1),
(9, 9, 'iphone SE', 1000, 1),
(10, 10, 'iphone SE', 1000, 1),
(11, 11, 'Pavilion 2000', 1200, 1),
(12, 12, 'Pavilion 2000', 1200, 1),
(13, 12, '4G modem', 260, 3),
(14, 13, 'Washing Machine', 2000, 1),
(15, 13, '4G modem', 260, 4),
(16, 13, 'Samsung Galaxy S8', 2000, 1),
(17, 14, 'iphone SE', 1000, 1),
(18, 14, 'hdmi 1.4', 100, 1),
(19, 15, 'Pavilion 2000', 1200, 1),
(20, 15, 'hdmi 1.4', 100, 1),
(21, 16, 'Pavilion 2000', 1200, 1),
(22, 16, 'Pavilion 2000', 1200, 1),
(23, 16, 'Samsung Galaxy S8', 2000, 1),
(24, 17, 'Washing Machine', 2000, 1),
(25, 17, 'iphone SE', 1000, 1),
(26, 17, 'Washing Machine', 2000, 1),
(27, 17, 'hdmi 1.4', 100, 1);

-- --------------------------------------------------------

--
-- Table structure for table `products`
--

CREATE TABLE `products` (
  `pid` int(11) NOT NULL,
  `cid` int(11) NOT NULL,
  `bid` int(11) NOT NULL,
  `product_name` varchar(100) NOT NULL,
  `product_price` double NOT NULL,
  `product_stock` int(11) NOT NULL,
  `added_date` date NOT NULL,
  `p_status` enum('1','0') NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `products`
--

INSERT INTO `products` (`pid`, `cid`, `bid`, `product_name`, `product_price`, `product_stock`, `added_date`, `p_status`) VALUES
(1, 6, 1, 'Samsung Galaxy S8', 2000, 556, '2019-03-25', '1'),
(8, 6, 4, 'iphone SE', 1000, 250, '2019-03-21', '1'),
(9, 12, 3, 'Pavilion 2000', 1200, 229, '2019-03-22', '1'),
(11, 1, 1, 'Washing Machine', 2000, 27, '2019-03-26', '1'),
(13, 4, 1, '4G modem', 260, 493, '2019-03-25', '1'),
(14, 1, 1, 'hdmi 1.4', 100, 47, '2019-03-25', '1'),
(15, 1, 1, 'remote', 35.5, 250, '2019-03-26', '1'),
(16, 4, 7, 'router', 458.5, 35, '2019-03-26', '1');

-- --------------------------------------------------------

--
-- Table structure for table `users`
--

CREATE TABLE `users` (
  `id` int(11) NOT NULL,
  `username` varchar(255) NOT NULL,
  `email` varchar(255) NOT NULL,
  `password` varchar(255) NOT NULL,
  `usertype` enum('Admin','Other','','') NOT NULL,
  `register_date` date NOT NULL,
  `last_login` datetime NOT NULL,
  `notes` text NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `users`
--

INSERT INTO `users` (`id`, `username`, `email`, `password`, `usertype`, `register_date`, `last_login`, `notes`) VALUES
(1, 'Admino', 'admin@admin.com', '$2y$08$9yMz9tNsxZhhiuhzCRzuSeEfU2vTrGMWT3jmeU/MANFBBtv7M/BSW', 'Admin', '2019-03-20', '2019-04-10 13:08:02', ''),
(2, 'Boss Terror', 'tagor@gmail.com', '$2y$08$fOI79mXDA05L4NJzL73edeY/u7TS5NbvTzmXCAq0LaZdsS9gyx.5C', 'Admin', '2019-03-20', '2019-03-20 20:21:07', ''),
(3, 'gengis', 'khan@gmail.com', '$2y$08$dRbCelyBMHsgd2kvqXWfmeL3zf5TmPlrFHSbEoENQwC.7ZtJAGo7S', 'Admin', '2019-03-21', '2019-03-21 13:45:28', '');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `brands`
--
ALTER TABLE `brands`
  ADD PRIMARY KEY (`bid`),
  ADD UNIQUE KEY `brand_name` (`brand_name`);

--
-- Indexes for table `categories`
--
ALTER TABLE `categories`
  ADD PRIMARY KEY (`cid`),
  ADD UNIQUE KEY `category_name` (`category_name`);

--
-- Indexes for table `invoice`
--
ALTER TABLE `invoice`
  ADD PRIMARY KEY (`invoice_no`);

--
-- Indexes for table `invoice_details`
--
ALTER TABLE `invoice_details`
  ADD PRIMARY KEY (`id`),
  ADD KEY `invoice_no` (`invoice_no`);

--
-- Indexes for table `products`
--
ALTER TABLE `products`
  ADD PRIMARY KEY (`pid`),
  ADD UNIQUE KEY `product_name` (`product_name`),
  ADD KEY `cid` (`cid`),
  ADD KEY `bid` (`bid`);

--
-- Indexes for table `users`
--
ALTER TABLE `users`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `email` (`email`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `brands`
--
ALTER TABLE `brands`
  MODIFY `bid` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=20;

--
-- AUTO_INCREMENT for table `categories`
--
ALTER TABLE `categories`
  MODIFY `cid` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=21;

--
-- AUTO_INCREMENT for table `invoice`
--
ALTER TABLE `invoice`
  MODIFY `invoice_no` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=18;

--
-- AUTO_INCREMENT for table `invoice_details`
--
ALTER TABLE `invoice_details`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=28;

--
-- AUTO_INCREMENT for table `products`
--
ALTER TABLE `products`
  MODIFY `pid` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=17;

--
-- AUTO_INCREMENT for table `users`
--
ALTER TABLE `users`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- Constraints for dumped tables
--

--
-- Constraints for table `invoice_details`
--
ALTER TABLE `invoice_details`
  ADD CONSTRAINT `invoice_details_ibfk_1` FOREIGN KEY (`invoice_no`) REFERENCES `invoice` (`invoice_no`);

--
-- Constraints for table `products`
--
ALTER TABLE `products`
  ADD CONSTRAINT `products_ibfk_1` FOREIGN KEY (`cid`) REFERENCES `categories` (`cid`),
  ADD CONSTRAINT `products_ibfk_2` FOREIGN KEY (`bid`) REFERENCES `brands` (`bid`);
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
